package dao;

import java.math.BigDecimal;
import java.util.List;

import domain.Order;
import org.junit.Test;

import static domain.Order.Type.BUY;
import static domain.Order.Type.SELL;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.UUID.randomUUID;
import static org.junit.Assert.*;

public class OrderDaoTest {

    private static final Order BUY_ORDER_1 = new Order(randomUUID(), new BigDecimal("1.1"), ONE, BUY);
    private static final Order BUY_ORDER_2 = new Order(randomUUID(), new BigDecimal("1.2"), TEN, BUY);
    private static final Order SELL_ORDER_1 = new Order(randomUUID(), new BigDecimal("1.3"), ONE, SELL);
    private static final Order SELL_ORDER_2 = new Order(randomUUID(), new BigDecimal("1.4"), TEN, SELL);

    private final OrderDao unit = new OrderDao();


    @Test
    public void register_shouldAddOrder() throws Exception {

        unit.register(BUY_ORDER_1);

        List<Order> expected = singletonList(BUY_ORDER_1);
        assertEquals(expected, unit.getOrders(BUY));

    }

    @Test
    public void cancel_shouldReturnFalse_whenOrderDoesNotExist() throws Exception {
        unit.register(BUY_ORDER_2);
        boolean actual = unit.cancel(BUY_ORDER_1);

        assertFalse(actual);
        assertEquals(singletonList(BUY_ORDER_2), unit.getOrders(BUY));
    }

    @Test
    public void cancel_shouldRemoveOrderAndReturnTrue_whenOrderDoesExist() throws Exception {
        unit.register(BUY_ORDER_2);
        boolean actual = unit.cancel(BUY_ORDER_2);

        assertTrue(actual);
        assertTrue(unit.getOrders(BUY).isEmpty());
    }

    @Test
    public void cancel_shouldRemoveSingleInstanceOfAnOrderAndReturnTrue_whenDuplicateOrdersExist() throws Exception {

        unit.register(BUY_ORDER_2);
        unit.register(BUY_ORDER_2);
        unit.register(BUY_ORDER_2);

        boolean actual = unit.cancel(BUY_ORDER_2);

        assertTrue(actual);
        List<Order> expectedOrders = asList(BUY_ORDER_2, BUY_ORDER_2);  //  2 remaining orders

        assertEquals(expectedOrders, unit.getOrders(BUY));
    }

    @Test
    public void getOrders_shouldReturnEmptyList_whenThereAreNoOrders() throws Exception {
        assertTrue(unit.getOrders(BUY).isEmpty());
        assertTrue(unit.getOrders(SELL).isEmpty());
    }

    @Test
    public void getOrders_shouldFilterBuyOrders_whenThereAreMultipleOrders() throws Exception {
        unit.register(BUY_ORDER_1);
        unit.register(SELL_ORDER_1);
        unit.register(BUY_ORDER_2);
        unit.register(SELL_ORDER_2);

        List<Order> actual = unit.getOrders(BUY);

        List<Order> expected = asList(BUY_ORDER_1, BUY_ORDER_2);

        assertEquals(expected, actual);
    }

    @Test
    public void getOrders_shouldFilterSellOrders_whenThereAreMultipleOrders() throws Exception {
        unit.register(BUY_ORDER_1);
        unit.register(SELL_ORDER_1);
        unit.register(BUY_ORDER_2);
        unit.register(SELL_ORDER_2);

        List<Order> actual = unit.getOrders(SELL);

        List<Order> expected = asList(SELL_ORDER_1, SELL_ORDER_2);

        assertEquals(expected, actual);
    }

}
