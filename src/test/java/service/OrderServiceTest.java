package service;

import java.math.BigDecimal;
import java.util.List;

import dao.OrderDao;
import domain.Order;
import domain.OrderSummary;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static domain.Order.Type.BUY;
import static domain.Order.Type.SELL;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.UUID.randomUUID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

// NOTE: not testing for null parameter values as they are assumed to be mandatory (otherwise I would use java.util.Optional)

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Mock
    private OrderDao mockOrderDao;

    @InjectMocks
    private OrderService unit;

    @Test
    public void register_shouldCallDaoToRegister_whenValidOrder() throws Exception {
        Order order = new Order(randomUUID(), TEN, ONE, BUY);

        unit.register(order);

        verify(mockOrderDao).register(order);
        verifyNoMoreInteractions(mockOrderDao);
    }

    @Test
    public void cancel_shouldReturnTrue_whenDaoReturnsSuccess() throws Exception {
        Order order = new Order(randomUUID(), TEN, ONE, BUY);
        when(mockOrderDao.cancel(order)).thenReturn(true);
        assertTrue(unit.cancel(order));
    }

    @Test
    public void cancel_shouldReturnFalse_whenDaoReturnsFailure() throws Exception {
        Order order = new Order(randomUUID(), TEN, ONE, BUY);
        when(mockOrderDao.cancel(order)).thenReturn(false);
        assertFalse(unit.cancel(order));
    }


    @Test
    public void getSummary_shouldReturnEmptyList_whenThereAreNoOrders() throws Exception {
        when(mockOrderDao.getOrders(BUY)).thenReturn(emptyList());

        List<OrderSummary> actual = unit.getSummary(BUY);

        assertTrue(actual.isEmpty());
        verify(mockOrderDao).getOrders(BUY);
        verifyNoMoreInteractions(mockOrderDao);
    }

    @Test
    public void getSummary_shouldReturnExpectedOrderSummariesInDescendingOrder_whenThereAreNonDuplicatePricesForBuyOrders() throws Exception {

        //  when the OrderDao returns this list of live buy orders
        List<Order> liveOrders = asList(
                new Order(randomUUID(), new BigDecimal("1.10"), ONE, BUY),
                new Order(randomUUID(), new BigDecimal("1.20"), ONE, BUY),
                new Order(randomUUID(), new BigDecimal("1.30"), ONE, BUY),
                new Order(randomUUID(), new BigDecimal("1.40"), ONE, BUY)
        );
        when(mockOrderDao.getOrders(BUY)).thenReturn(liveOrders);


        List<OrderSummary> actual = unit.getSummary(BUY);

        //  then this list of orderSummary object will be returned (reverse order)
        List<OrderSummary> expected = asList(
                new OrderSummary(new BigDecimal("1.40"), ONE, BUY),
                new OrderSummary(new BigDecimal("1.30"), ONE, BUY),
                new OrderSummary(new BigDecimal("1.20"), ONE, BUY),
                new OrderSummary(new BigDecimal("1.10"), ONE, BUY)
        );

        assertEquals(expected, actual);

        verify(mockOrderDao).getOrders(BUY);
        verifyNoMoreInteractions(mockOrderDao);
    }

    @Test
    public void getSummary_shouldMergeDuplicatePricesAndReturnSummariesInDescendingOrder_whenThereAreDuplicatePricesForBuyOrders() throws Exception {

        //  when the OrderDao returns this list of live buy orders
        List<Order> liveOrders = asList(
                new Order(randomUUID(), new BigDecimal("1.10"), ONE, BUY),
                new Order(randomUUID(), new BigDecimal("1.10"), ONE, BUY),
                new Order(randomUUID(), new BigDecimal("1.30"), ONE, BUY),
                new Order(randomUUID(), new BigDecimal("1.30"), TEN, BUY),
                new Order(randomUUID(), new BigDecimal("1.30"), ONE, BUY)
        );
        when(mockOrderDao.getOrders(BUY)).thenReturn(liveOrders);


        List<OrderSummary> actual = unit.getSummary(BUY);

        //  then this list of orderSummary object will be returned (reverse order)
        List<OrderSummary> expected = asList(
                new OrderSummary(new BigDecimal("1.30"), new BigDecimal("12"), BUY),
                new OrderSummary(new BigDecimal("1.10"), new BigDecimal("2"), BUY)
        );

        assertEquals(expected, actual);

        verify(mockOrderDao).getOrders(BUY);
        verifyNoMoreInteractions(mockOrderDao);
    }

    @Test
    public void getSummary_shouldReturnExpectedOrderSummariesInAscendingOrder_whenThereAreNonDuplicatePricesForSellOrders() throws Exception {

        //  when the OrderDao returns this list of live buy orders
        List<Order> liveOrders = asList(
                new Order(randomUUID(), new BigDecimal("1.40"), ONE, SELL),
                new Order(randomUUID(), new BigDecimal("1.30"), ONE, SELL),
                new Order(randomUUID(), new BigDecimal("1.10"), ONE, SELL),
                new Order(randomUUID(), new BigDecimal("1.20"), ONE, SELL)
        );
        when(mockOrderDao.getOrders(SELL)).thenReturn(liveOrders);


        List<OrderSummary> actual = unit.getSummary(SELL);

        //  then this list of orderSummary object will be returned (reverse order)
        List<OrderSummary> expected = asList(
                new OrderSummary(new BigDecimal("1.10"), ONE, SELL),
                new OrderSummary(new BigDecimal("1.20"), ONE, SELL),
                new OrderSummary(new BigDecimal("1.30"), ONE, SELL),
                new OrderSummary(new BigDecimal("1.40"), ONE, SELL)
        );

        assertEquals(expected, actual);

        verify(mockOrderDao).getOrders(SELL);
        verifyNoMoreInteractions(mockOrderDao);
    }

    @Test
    public void getSummary_shouldMergeDuplicatePricesAndReturnSummariesInAscendingOrder_whenThereAreDuplicatePricesForSellOrders() throws Exception {

        //  when the OrderDao returns this list of live buy orders
        List<Order> liveOrders = asList(
                new Order(randomUUID(), new BigDecimal("1.10"), ONE, SELL),
                new Order(randomUUID(), new BigDecimal("1.10"), ONE, SELL),
                new Order(randomUUID(), new BigDecimal("1.30"), ONE, SELL),
                new Order(randomUUID(), new BigDecimal("1.30"), TEN, SELL),
                new Order(randomUUID(), new BigDecimal("1.30"), ONE, SELL)
        );
        when(mockOrderDao.getOrders(SELL)).thenReturn(liveOrders);


        List<OrderSummary> actual = unit.getSummary(SELL);

        //  then this list of orderSummary object will be returned (reverse order)
        List<OrderSummary> expected = asList(
                new OrderSummary(new BigDecimal("1.10"), new BigDecimal("2"), SELL),
                new OrderSummary(new BigDecimal("1.30"), new BigDecimal("12"), SELL)
        );

        assertEquals(expected, actual);

        verify(mockOrderDao).getOrders(SELL);
        verifyNoMoreInteractions(mockOrderDao);
    }

}
