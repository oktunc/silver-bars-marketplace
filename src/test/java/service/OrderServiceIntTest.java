package service;

import java.math.BigDecimal;
import java.util.List;

import dao.OrderDao;
import domain.Order;
import domain.OrderSummary;
import org.junit.Before;
import org.junit.Test;

import static domain.Order.Type.BUY;
import static domain.Order.Type.SELL;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.UUID.randomUUID;
import static org.junit.Assert.*;

public class OrderServiceIntTest {

    private static final Order BUY_ORDER_1 = new Order(randomUUID(), new BigDecimal("1.1"), ONE, BUY);
    private static final Order BUY_ORDER_2 = new Order(randomUUID(), new BigDecimal("1.2"), TEN, BUY);
    private static final Order SELL_ORDER_1 = new Order(randomUUID(), new BigDecimal("1.3"), ONE, SELL);
    private static final Order SELL_ORDER_2 = new Order(randomUUID(), new BigDecimal("1.4"), TEN, SELL);

    private final OrderDao orderDao = new OrderDao();
    private OrderService orderService;

    @Before
    public void setUp() throws Exception {
        orderService = new OrderService(orderDao);
    }

    @Test
    public void register_shouldAddOrdersIntoStore() throws Exception {

        orderService.register(BUY_ORDER_1);
        orderService.register(SELL_ORDER_1);

        assertEquals(singletonList(BUY_ORDER_1), orderDao.getOrders(BUY));
        assertEquals(singletonList(SELL_ORDER_1), orderDao.getOrders(SELL));
    }

    @Test
    public void cancel_shouldRemoveOrdersFromStore() throws Exception {
        orderService.register(BUY_ORDER_1);
        orderService.register(BUY_ORDER_2);
        orderService.register(SELL_ORDER_1);
        orderService.register(SELL_ORDER_2);

        boolean cancelledBuyOrder = orderService.cancel(BUY_ORDER_2);
        boolean cancelledSellOrder = orderService.cancel(SELL_ORDER_2);

        assertTrue(cancelledBuyOrder);
        assertTrue(cancelledSellOrder);
        assertEquals(singletonList(BUY_ORDER_1), orderDao.getOrders(BUY));
        assertEquals(singletonList(SELL_ORDER_1), orderDao.getOrders(SELL));
    }

    @Test
    public void getSummary_shouldReturnMergedOrdersInAscendingOrderForSellType() throws Exception {
        registerOrders(SELL);

        List<OrderSummary> actual = orderService.getSummary(SELL);

        List<OrderSummary> expected = asList(
                new OrderSummary(new BigDecimal("306"), new BigDecimal("5.5"), SELL),
                new OrderSummary(new BigDecimal("307"), new BigDecimal("1.5"), SELL),
                new OrderSummary(new BigDecimal("310"), new BigDecimal("1.2"), SELL)
        );
        assertEquals(expected, actual);
    }



    @Test
    public void getSummary_shouldReturnMergedOrdersInDescendingOrderForBuyType() throws Exception {
        registerOrders(BUY);

        List<OrderSummary> actual = orderService.getSummary(BUY);

        List<OrderSummary> expected = asList(
                new OrderSummary(new BigDecimal("310"), new BigDecimal("1.2"), BUY),
                new OrderSummary(new BigDecimal("307"), new BigDecimal("1.5"), BUY),
                new OrderSummary(new BigDecimal("306"), new BigDecimal("5.5"), BUY)
        );

        assertEquals(expected, actual);
    }
    private void registerOrders(Order.Type type) {
        orderService.register(new Order(randomUUID(), new BigDecimal("306"), new BigDecimal("3.5"), type));
        orderService.register(new Order(randomUUID(), new BigDecimal("310"), new BigDecimal("1.2"), type));
        orderService.register(new Order(randomUUID(), new BigDecimal("307"), new BigDecimal("1.5"), type));
        orderService.register(new Order(randomUUID(), new BigDecimal("306"), new BigDecimal("2.0"), type));
    }

}
