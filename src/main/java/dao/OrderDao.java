package dao;

import java.util.ArrayList;
import java.util.List;

import domain.Order;
import domain.Order.Type;

import static java.util.Collections.synchronizedList;
import static java.util.stream.Collectors.toList;

public class OrderDao {

    private final List<Order> orders = synchronizedList(new ArrayList<>());

    public void register(Order order) {
        orders.add(order);
    }

    public boolean cancel(Order order) {
        return orders.remove(order);
    }

    public List<Order> getOrders(Type type) {
        return orders.stream()
                .filter(order -> order.getOrderType() == type)
                .collect(toList());
    }
}
