package domain;

import java.math.BigDecimal;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

public class OrderSummary {

    private final BigDecimal quantity;
    private final BigDecimal price;
    private final Order.Type orderType;


    public OrderSummary(BigDecimal price, BigDecimal quantity, Order.Type orderType) {
        this.quantity = quantity;
        this.price = price;
        this.orderType = orderType;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Order.Type getOrderType() {
        return orderType;
    }

    @Override
    public boolean equals(Object o) {
        return reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return reflectionToString(this);

    }
}
