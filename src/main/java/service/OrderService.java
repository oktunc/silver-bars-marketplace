package service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import dao.OrderDao;
import domain.Order;
import domain.Order.Type;
import domain.OrderSummary;

import static domain.Order.Type.SELL;
import static java.math.BigDecimal.ZERO;
import static java.util.stream.Collectors.*;

public class OrderService {

    private final OrderDao orderDao;

    public OrderService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    public void register(Order order) {
        orderDao.register(order);
    }

    public boolean cancel(Order order) {
        return orderDao.cancel(order);
    }

    public List<OrderSummary> getSummary(Type type) {

        List<Order> orders = orderDao.getOrders(type);

        List<OrderSummary> results = new ArrayList<>();

        orders.stream()
                .collect(groupingBy(Order::getPrice, mapping(Order::getQuantity, toList())))                 // group them by price
                .forEach((price, quantities) -> results.add(buildOrderSummary(type, price, quantities)));    // sum up the quantities

        sortOrderSummaries(type, results);

        return results;
    }

    private void sortOrderSummaries(Type type, List<OrderSummary> orderSummaries) {
        Comparator<OrderSummary> sellOrderComparator = Comparator.comparing(OrderSummary::getPrice);

        if (type == SELL) {
            orderSummaries.sort(sellOrderComparator);
        } else {
            orderSummaries.sort(sellOrderComparator.reversed());
        }
    }

    private OrderSummary buildOrderSummary(Type type, BigDecimal price, List<BigDecimal> quantities) {
        BigDecimal quantitiesSum = quantities.stream().reduce(ZERO, BigDecimal::add);
        return new OrderSummary(price, quantitiesSum, type);
    }
}
